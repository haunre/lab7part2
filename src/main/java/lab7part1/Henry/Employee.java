/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab7part1.Henry;

/**
 *
 * @author 123
 */
public class Employee {
    protected String name;
    protected double hourlyWage;
    protected double noOfHoursWorked;

    //COnstructor
    public Employee(String name, double hourlyWage, double noOfHoursWorked) {
        this.name = name;
        this.hourlyWage = hourlyWage;
        this.noOfHoursWorked = noOfHoursWorked;
    }
    
    //Getters

    public String getName() {
        return name;
    }

    public double getHourlyWage() {
        return hourlyWage;
    }

    public double getNoOfHoursWorked() {
        return noOfHoursWorked;
    }
    
    public double calculatePay(){
            return getHourlyWage() * getNoOfHoursWorked();
    }
    
}
