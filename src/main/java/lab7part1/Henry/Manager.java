/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab7part1.Henry;

/**
 *
 * @author 123
 */
public class Manager extends Employee {
    
    private double bonus;

    public Manager(double bonus, String name, double hourlyWage, double noOfHoursWorked) {
        super(name, hourlyWage, noOfHoursWorked);
        this.bonus = bonus;
    }

    public double getBonus() {
        return bonus;
    }
    
    @Override
    public double calculatePay(){
        return super.calculatePay() + getBonus();
    }
}
