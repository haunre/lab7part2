/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab7part1.Henry;

/**
 *
 * @author 123
 */
public class PayrollSimulation {
    
    public static void main(String[] args) {
        Employee emp = new Employee("Dave", 15, 35);
        Employee mng = new Manager(10, "Andrew", 37, 30);
        
        System.out.println(emp.getName() + "paycheck in $" + emp.calculatePay());
        System.out.println(mng.getName() + "paycheck in $" + mng.calculatePay());
        
    }
        
        
    
    
}
